import jwt from 'jsonwebtoken';
import config from '../config';

export default {
  isAuthenticated: (req, res, next) => {
  	let token = req.header('Authorization')
  	if (token) {
      token = token.split(' ')[1]
  		jwt.verify(token, config.jwt.secret, function(err, decoded) {
    		if (err) {
    			res.json({
    				'status': 'error',
    				'error': 'invalid token'
    			});
    		} else {
          req.user = decoded
    			next();
    		}
    	});
  	} else {
      res.json({
        'status': 'error',
        'error': 'token is required'
      });
    }
  },
  isAdmin: (req, res, next) => {
  	let user = req.user
    if (!user.is_admin) {
      res.json({
        'status': 'error',
        'error': 'you are not authorized to perform this action'
      });
    } else {
      next();
    }
  },
};
