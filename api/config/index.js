export default {
  db: {
    user: 'postgres',
    host: 'localhost',
    database: 'way_farer',
    password: 'example',
    port: 5432,
  },
  jwt: {
    secret: '12345',
  },
};
