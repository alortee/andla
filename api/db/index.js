import { Pool } from 'pg';
import config from '../config';

const { db } = config;

const pool = new Pool({
  user: db.user,
  host: db.host,
  database: db.database,
  password: db.password,
  port: db.port,
});

const usersDDL = `
CREATE TABLE IF NOT EXISTS
  users(
    id serial PRIMARY KEY,
    email VARCHAR(191) UNIQUE NOT NULL,
    password VARCHAR(191) UNIQUE NOT NULL,
    first_name VARCHAR(191) NOT NULL,
    last_name VARCHAR(191) NOT NULL,
    is_admin BOOLEAN NOT NULL,
    created_at TIMESTAMP NOT NULL
  )`;

const busesDDL = `
CREATE TABLE IF NOT EXISTS
  buses(
    id serial PRIMARY KEY,
    number_plate VARCHAR(191) NOT NULL,
    manufacturer VARCHAR(191) NOT NULL,
    model VARCHAR(191) NOT NULL,
    year VARCHAR(191) NOT NULL,
    capacity INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL
  )`;


const tripsDDL = `
CREATE TABLE IF NOT EXISTS
  trips(
    id serial PRIMARY KEY,
    bus_id INTEGER NOT NULL,
    origin VARCHAR(191) NOT NULL,
    destination VARCHAR(191) NOT NULL,
    trip_date TIMESTAMP NOT NULL,
    fare FLOAT NOT NULL,
    status VARCHAR(191) NOT NULL,
    created_at TIMESTAMP NOT NULL
  )`;

const bookingsDDL = `
CREATE TABLE IF NOT EXISTS
  bookings(
    id serial PRIMARY KEY,
    trip_id INTEGER NOT NULL,
    user_id INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL
  )`;


pool.query(usersDDL, (err, res) => {
  if (res) { console.log('created the users database'); }
  if (err) { console.log(err); }
});

pool.query(busesDDL, (err, res) => {
  if (res) { console.log('created the buses database'); }
  if (err) { console.log(err); }
});

pool.query(tripsDDL, (err, res) => {
  if (res) { console.log('created the trips database'); }
  if (err) { console.log(err); }
});

pool.query(bookingsDDL, (err, res) => {
  if (res) { console.log('created the bookings database'); }
  if (err) { console.log(err); }
});

export default pool;
