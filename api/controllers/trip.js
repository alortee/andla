import db from '../db';
import config from '../config';

export default {
  async createTrip(req, res) {
    const body = req.body
    let query = {
      text: `
        INSERT INTO 
        trips(
          bus_id, 
          origin, 
          destination, 
          trip_date, 
          fare,
          status,
          created_at) 
        VALUES($1, $2, $3, $4, $5, $6, $7) RETURNING *`,
      values: [
        body.bus_id,
        body.origin,
        body.destination,
        new Date(), // supposed to be -> body.trip_date
        body.fare,
        'active',
        new Date(),
      ],
    };
    
    let result = await db.query(query);

  	return res.json({
  		status: 'success',
  		data: result.rows[0]
  	})
  },
  async allTrips(req, res) {
  	let query = {
	    text: 'SELECT * FROM trips',
	    values: [],
	  };

    let result = await db.query(query);
  	return res.json({
  		status: 'success',
  		data: result.rows
  	})
  },
  async cancelTrip(req, res) {
  	return res.json({
  		status: 'success',
  		data: {
  			
  		}
  	})
  },
};
