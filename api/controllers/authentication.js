import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import db from '../db';
import config from '../config';

export default {
  async signup(req, res) {
    const { body } = req;

    try {
      // check if the user with that email already exists
      // before you continue
      // //
      let query = {
        text: 'SELECT COUNT(*) FROM users WHERE email=$1',
        values: [body.email],
      };

      let result = await db.query(query);
      const { count } = result.rows[0];

      if (count > 0) {
        return res.json({
          status: 'error',
          error: 'user already exists',
        });
      }

      // hash the password
      const hashedPassword = await bcrypt.hash(body.password, 5);
      const data = {
        email: body.email,
        first_name: body.first_name,
        last_name: body.last_name,
        is_admin: false,
        created_at: new Date(),
      };

      // save the user at this point,
      // generate the jwt token containing the users details
      // and return response with the jwt token
      //
      // //
      query = {
        text: `
          INSERT INTO 
          users(
            email, 
            password, 
            first_name, 
            last_name, 
            is_admin, 
            created_at) 
          VALUES($1, $2, $3, $4, $5, $6) RETURNING id`,
        values: [
          data.email,
          hashedPassword,
          data.first_name,
          data.last_name,
          data.is_admin,
          data.created_at,
        ],
      };

      result = await db.query(query);
      const userId = result.rows[0].id;

      // create the JWT token
      const token = jwt.sign(data, config.jwt.secret);

      return res.json({
        status: 'success',
        data: {
          userId,
          is_admin: data.is_admin,
          token,
        },
      });
    } catch (error) {
      console.log(error);
      return res.json({
        status: 'error',
        data: {
          message: 'an error occured',
          error,
        },
      });
    }
  },

  async signin(req, res) {
    const { body } = req;


    const query = {
      text: 'SELECT * FROM users WHERE email=$1',
      values: [body.email],
    };

    const result = await db.query(query);

    if (result.rowCount === 0) {
      return res.json({
        status: 'error',
        error: 'username or password is not correct',
      });
    }

    const user = result.rows[0];
    const correctPassword = await bcrypt.compare(body.password, user.password);

    if (correctPassword === false) {
      return res.json({
        status: 'error',
        error: 'username or password is not correct',
      });
    }

    delete user.password;

    const token = jwt.sign(user, config.jwt.secret);
    return res.json({
      status: 'success',
      data: {
        user_id: user.id,
        is_admin: user.is_admin,
        token,
      },
    });
  },
};
