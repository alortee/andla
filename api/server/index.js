import express from 'express';
import bodyParser from 'body-parser';
import apiRouter from '../routes';
import db from '../db';

const port = process.env.PORT || 8000;
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api/v1', apiRouter);

app.use('*', (req, res) => res.status(404).send({ status: 'error', error: 'not found' }));

app.use((err, req, res) => {
  res.json({
    status: 'error',
    message: 'an error occured',
    error: err,
  });
});

app.listen(port, () => {
  console.log(`Server is running on PORT ${port}`);
});

export default app;
