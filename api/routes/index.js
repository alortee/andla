import { Router } from 'express';
import m from '../middlewares';

import authController from '../controllers/authentication';
import tripController from '../controllers/trip';
import bookingController from '../controllers/booking';

const router = Router();

router.post('/auth/signup', authController.signup);
router.post('/auth/signin', authController.signin);

router.post('/trips', m.isAuthenticated, m.isAdmin, tripController.createTrip);
router.get('/trips', m.isAuthenticated, tripController.allTrips);
router.patch('/trips/:id', m.isAuthenticated, m.isAdmin, tripController.cancelTrip);

router.post('/bookings', m.isAuthenticated, bookingController.createBooking);
router.get('/bookings', m.isAuthenticated, bookingController.allBookings);
router.patch('/bookings/:id', m.isAuthenticated, bookingController.cancelBooking);

export default router;
